'use strict'
const redis = require('redis')

let
  _arr = i => i instanceof Array ? i : [i],
  Promises = (array) => Promise
    .all(array)
    .then(v =>
      v.reduce((o, i) =>
        Object.assign(o, i), {})),
  num = (k) => isFinite(k) ? Number(k) : k

let instanceTypes = {
  disk: (cli, namespace) => ({
    get: (keys) => new Promise((res, rej) =>
      cli.mget((keys = _arr(keys)).map(i => i + namespace), (err, records) =>
        err
          ? rej(err)
          : res(records.reduce((o, record, index) =>
            record ? {
              ...o,
              [num(keys[index])]: JSON.parse(record)
            } : o
          , {})))),
    set: (obj, expire = 300) => new Promise((res, rej) => {
      let sets = Object.keys(obj)

      cli.mset(
        sets
          .reduce((j, i) =>
            j.concat([i + namespace, JSON.stringify(obj[i])]), []), (err) => err ? rej(err) : res(true))
      if(expire != -1) {
        sets
          .forEach(i => cli.expire(i + namespace, expire))
      }
    }),
    search: (query) =>
      new Promise((res, rej) =>
        cli.keys(query + '*' + namespace, (err, records) => console.log(query, err, records) || err
          ? rej(err)
          : res(records.map(i => i.replace(namespace, ''))))),
    rem: (keys) => new Promise((res, rej) =>
      cli.del(_arr(keys).map(i => i + namespace)) || res()),
    Model() {
      return new Proxy(this, {
        get: (o, id) => o[id] || o
          .get(id)
          .then(res => res[id] ? res[id] : Promise.reject('no record for ' + id + ' on ' + namespace)),
        set: (o, id, data) => o.set({[id]: data}, -1),
        deleteProperty: (o, id) => o.rem(id)
      })
    }
  }),
  set: (cli, namespace) => ({
    add: (key, members) =>
      new Promise((res, rej) =>
        cli.sadd([key + namespace].concat(members), (err, reply) => err ? rej(err) : res(reply))),
    rem: (key, members) =>
      new Promise((res, rej) =>
        cli.srem([key + namespace].concat(members), (err, reply) => err ? rej(err) : res(reply))),
    get: (key) =>
      new Promise((res, rej) =>
        cli.smembers(key + namespace, (err, reply) => err ? rej(err) : res(reply.map(num)))),
    Model: () => new Proxy(instanceTypes.set(cli, namespace), {
      get: (o, key) => o.get(key),
      set: (o, key, data) => o.add(key, data),
      deleteProperty: (o, key) => cli.del(key)
    })
  })
}
let defaultConf = {
  host: '127.0.0.1',
  port: 6379
}
module.exports = ({namespace, type = 'disk', ...conf}) => {
  namespace = namespace ? ':' + namespace : ''
  conf = {...defaultConf, ...conf}
  const Log = require('ilogr')
    .bind(null, 'redis' + namespace)
  let cli = redis.createClient(conf),
    instance = {
      available: false,
      cli,
      ...instanceTypes[type](cli, namespace)
    }

  cli.on('error', (err) => Log(err) && (instance.available = false))
  cli.on('ready', () => Log('ready') && (instance.available = true))
  return instance
}